import mypackage.Context;
import mypackage.Vcs;
import mypackage.ViewLeaveScreen;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws MalformedURLException, JAXBException {

        Context context = new Context();
        while(!(context.current_state instanceof ViewLeaveScreen))
        {
            context.current_state.display();
            context.current_state.listenInput();
            context.current_state.doUserRequest();
        }
        context.current_state.display();
    }

}
