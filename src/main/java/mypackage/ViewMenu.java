package mypackage;

public class ViewMenu extends State implements IState{

    ViewMenu(Context context) {
        super(context);
    }

    @Override
    public void display() {
        String header = writeHeader();
        String content = writeContent();
        String choices = writeChoices();



        System.out.println(header + content + choices);
    }

    @Override
    public void doUserRequest() {
        if (this.userInput.compareTo("q") == 0)
           this.context.setCurrent_state(new ViewLeaveScreen(this.context));
        if (this.userInput.compareTo("e") == 0)
            this.context.setCurrent_state(new ViewGetAdresse(this.context, "emprunter"));
    }


    public String writeHeader() {
        String header = "";//""********************************************************************************\n";
        // header += "Bienvenu !\n";
        //header += "********************************************************************************\n";

        return header;
    }

    public String writeContent() {
        String content = "";
        ModelStation model = ModelStation.getInstance();

        content = "LISTE DES STATIONS:\n\n";
        content += "Nom                                       | Velo disponibles | Places disponibles |\n";
        content += "-----------------------------------------------------------------------------------\n\n";

        for (Vcs.Sl.Si station : model.getStations())
        {
            int freeBikes = station.getFr();
            int parkingSpace = station.getAv();
            int totalfreeBikes = station.getTo();

            String freeBikeString = Integer.toString(freeBikes);
            String freeParkString = Integer.toString(parkingSpace);

            content += station.getNa() + getMarginSpaces(41, station.getNa()) + " | " + getMarginSpaces(16, freeBikeString) + freeBikes + " | " + getMarginSpaces(18, freeParkString) + parkingSpace + " | " + "\n";
        }
        return content;
    }

    public String writeChoices(){
        String choices = "\n'q' pour quitter,\n" +
                "'e' si vous voulez afficher les stations les plus proches.";

        return choices;
    }

}
