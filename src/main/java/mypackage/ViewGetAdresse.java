package mypackage;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;

public class ViewGetAdresse extends State implements IState{
    String action;

    ViewGetAdresse(Context context, String action) {
        super(context);
        this.action = action;
    }

    @Override
    public void display() {
        String header = "\nVeuillez saisir l'addresse ou vous vous trouver pour " + this.action + "\n";
        System.out.println(header);

    }

    @Override
    public void doUserRequest() {

        ModelStation.getInstance().setAddress(this.userInput);
        this.context.setCurrent_state(new ViewDisplayNearestStation(this.context));
    }
}
