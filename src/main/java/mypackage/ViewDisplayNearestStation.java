package mypackage;

import com.google.gson.*;
import com.google.gson.internal.bind.util.ISO8601Utils;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;

import java.io.IOException;
import java.sql.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewDisplayNearestStation extends State implements IState{
    ViewDisplayNearestStation(Context context) {
        super(context);
    }

    @Override
    public void display() {
        ArrayList<HashMap> stationsFromGmap = ModelStation.getInstance().getNearestLocations();
        //ModelStation.getInstance().getNearestLocationsWithFreeParks();

        String content = "";

        content = "Stations les plus proches :\n\n";
        content += "Nom                                       | Distance en Km | Duree du trajet | Velo disponibles | Places disponibles |\n";
        content += "---------------------------------------------------------------------------------------------------------------------\n\n";

        content += getStationLine(stationsFromGmap.get(0));
        content += getStationLine(stationsFromGmap.get(1));
        content += getStationLine(stationsFromGmap.get(2));

        String footer = "\n'q' pour quitter,\n" +
                "'c' pour changer d'adresse.";

        System.out.println(content + footer);
    }

    public String getStationLine(HashMap station){
       String stationContent = "" ;
        List<Vcs.Sl.Si> stations = ModelStation.getInstance().getStations();
        int stationId = Integer.valueOf(station.get("id").toString());
        String stationName = stations.get(stationId).getNa();
        String stationDistance = station.get("distance").toString();
        String stationTravelTime = station.get("duration").toString();
        String stationFreePark  = String.valueOf(stations.get(stationId).getAv());
        String stationFreeBike  = String.valueOf(stations.get(stationId).getFr());
        String stationTotalPark = String.valueOf(stations.get(stationId).getTo());

       stationContent += stationName + getMarginSpaces(41, stationName) + " | " + getMarginSpaces(14, stationDistance) + stationDistance + " | " + getMarginSpaces(15, stationTravelTime) + stationTravelTime + " | " + getMarginSpaces(16, stationFreeBike) + Integer.valueOf(stationFreeBike) + " | " + getMarginSpaces(17, stationFreePark) + stationFreePark + " |\n";



        return stationContent;
    }

    @Override
    public void doUserRequest() {
        if (this.userInput.compareTo("q") == 0)
            this.context.setCurrent_state(new ViewLeaveScreen(this.context));
        if (this.userInput.compareTo("c") == 0)
            this.context.setCurrent_state(new ViewGetAdresse(this.context, "emprunter"));
        if (this.userInput.compareTo("m") == 0)
            this.context.setCurrent_state(new ViewMenu(this.context));
    }
}
