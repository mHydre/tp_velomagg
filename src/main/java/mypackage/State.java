package mypackage;

import java.util.Scanner;

abstract public class State implements IState{
    protected String userInput;
    protected Context context;

    State(Context context){
       this.context = context;
    }

    public abstract void display();
    public abstract void doUserRequest();

    public void listenInput()
    {
        Scanner scan = new Scanner(System.in);
        String userInput = scan.nextLine();
        this.userInput = userInput;
    };

    protected String getColorRepresentationRatio(int numerateur, int denominateur){
        String ANSI_RED = "\u000B[31m";
        String ANSI_GREEN = "\u000B[32m";
        String ANSI_RESET = "\u000B[0m";
        String pimp_string = "";

        float ratio = (float) numerateur / (float) denominateur;

        String ratioString = String.valueOf(numerateur) + " / " + String.valueOf(denominateur);

        if (ratio <= -1.33 || numerateur < 6)
            pimp_string = ANSI_RED + ratioString + ANSI_RESET;
        else
            pimp_string = ANSI_GREEN + ratioString + ANSI_RESET;


        return pimp_string;
    }

    protected String getMarginSpaces(int max_char, String stationName){
        int string_lenght = stationName.length();
        String spaceString = " ".repeat(max_char - string_lenght);

        return spaceString;
    }
}
