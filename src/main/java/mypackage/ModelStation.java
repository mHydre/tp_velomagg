package mypackage;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class ModelStation {
    private static ModelStation instance;
    public String address = "";
    List<Vcs.Sl.Si> listStations;

    private ModelStation(){
        try {
            this.loadData();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static ModelStation getInstance() {
        if (instance == null) {
            instance = new ModelStation();
        }

        return instance;
    }

    public void loadData() throws JAXBException, MalformedURLException {
        JAXBContext jc = JAXBContext.newInstance("mypackage");
        Unmarshaller u = jc.createUnmarshaller();
        Vcs stations = (Vcs)u.unmarshal(new URL("https://data.montpellier3m.fr/sites/default/files/ressources/TAM_MMM_VELOMAG.xml"));
        this.listStations = stations.getSl().getSi();
    }

    public List<Vcs.Sl.Si> getStations()
    {
        return this.listStations;
    }

    public void setAddress(String address){
        this.address = address;
    }
    public LatLng[] getStationsLocations(){
       List<Vcs.Sl.Si> stations = getStations();
       int stationsCount = stations.size();

       LatLng[] stationsLocations = new LatLng[24];

       int index = 0;
       for(Vcs.Sl.Si station : getStations())
        {
            if (index < 24) {
                stationsLocations[index] = new LatLng(station.getLa().doubleValue(), station.getLg().doubleValue());
            }
            index++;
        }

       return stationsLocations;
    }

    public DistanceMatrix getMatrixResponse(LatLng[] stationsLocations, String adresse){
        final String API_KEY = "AIzaSyAZ_YCOs_iOAhcbkLzjxxe0mNvUclZPBNg";

        GeoApiContext context = new GeoApiContext.Builder().apiKey(API_KEY).build();
        DistanceMatrixApiRequest matrix = DistanceMatrixApi.newRequest(context);
        DistanceMatrix request = null;

        try {
            request = matrix
                    .destinations(stationsLocations)
                    .origins(adresse)
                    .mode(TravelMode.WALKING)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return request;
    }
    public ArrayList<HashMap> getNearestLocations(){
        String adresse = this.address;
        LatLng[] stationsLocations = this.getStationsLocations();
        DistanceMatrix answer = getMatrixResponse(stationsLocations, adresse);
        List<Vcs.Sl.Si> stations = getStations();
        ArrayList<HashMap> stationsDopped = new ArrayList<>();
        HashMap<String,Object> elementData = new HashMap<>();
        HashMap<String,Object> elementTravelData = new HashMap<>();

        int index = 0;
        for(DistanceMatrixElement element : answer.rows[0].elements){
            elementTravelData = new HashMap<>();

            elementTravelData.put("distance", element.distance.toString().replace(" km", ""));
            elementTravelData.put("duration", element.duration.toString().replace(" hour ", " heure "));
            elementTravelData.put("id", index);

            stationsDopped.add(elementTravelData);

            index++;
        }

        //stationsDopped.entrySet().stream().sorted()
        //System.out.println(answer.rows[1].elements.toString());//toString());
        //System.out.println(stationsDopped.get("1").get("2"));//toString());
        Collections.sort(stationsDopped, (a,b) -> ((Double.valueOf(a.get("distance").toString()) < Double.valueOf(b.get("distance").toString())) ? -1 : 1));
        return stationsDopped;
    }
}
