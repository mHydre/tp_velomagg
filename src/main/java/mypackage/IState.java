package mypackage;

public interface IState {
    public void display();
    public void doUserRequest();
    public void listenInput();
}
