package mypackage;

public class ViewLeaveScreen extends State implements IState{

    ViewLeaveScreen(Context context) {
        super(context);
    }

    @Override
    public void display() {
        String header = "**********************************************************************\n";
        String content = "A bientot !\n";
        String footer = "**********************************************************************\n";
        System.out.println(header + content + footer);
    }

    @Override
    public void doUserRequest() {

    }
}
